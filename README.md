# Jiden

[![Crates.io Version](https://img.shields.io/crates/v/jiden)](https://crates.io/crates/jiden)
[![docs.rs](https://img.shields.io/docsrs/jiden)](https://docs.rs/jiden/latest/jiden/)

Save and retrive state and progress through a system file

## Examples

### State Save

```rust
let state_saver = StateSaver::new("state.txt");
        
state_saver.save(&1); 
assert_eq!(state_saver.state(), Some(1));
```

After restarting the application:

```rust
let state_saver = StateSaver::new("state.txt");

assert_eq!(state_saver.state(), Some(1));
```

### Progress Save

```rust
let state_saver = ProgressSaver::new("state.txt");
        
state_saver.save(0, 1); 
assert_eq!(HashMap::from([(0, 1)]), progress_saver.state());
```

After restarting the application:

```rust
let state_saver = ProgressSaver::new("state.txt");

assert_eq!(HashMap::from([(0, 1)]), progress_saver.state());
```